import './App.css';
import React from "react";
import {Switch, BrowserRouter, Route} from 'react-router-dom'
import Connexion from "./pages/Connexion";
import MenuEleve from "./pages/MenuEleve";
import Cours from "./pages/Cours";
import Avatar from "./pages/Avatar";
import Jeux from "./pages/Jeux";
import Visio from "./pages/Visio";
import MenuProf from "./pages/MenuProf";
import DepotCours from "./pages/DepotCours";
import Navbar from './components/navbar/index';
=======
import Navbar from './components/navbar/index';
import DepotCours from './pages/DepotCours';


>>>>>>> 705a1692a15edd2ae860537ab5fcff4e2282b370

const App = () => {
  return (
    <BrowserRouter>
    <Navbar />
      <Switch> 
        <Route path="/" exact component={Connexion} />
        <Route path="/menuEleve" exact component={MenuEleve} />
        <Route path="/cours" exact component={Cours} />
        <Route path="/jeux" exact component={Jeux} />
        <Route path="/avatar" exact component={Avatar} />
        <Route path="/visio" exact component={Visio} />
        <Route path="/menuProf" exact component={MenuProf} />
        <Route path="/depot" exact component={DepotCours} />
      </Switch>
    </BrowserRouter>
  );
}

export default App;
