import React from 'react';
import Navigation from "../components/Navigation";
import Logo from "../components/Logo";


const Connexion = () => {
    return(
        <div className = "connexion">
            <div className = "box">
                <div className="columns is-full is-mobile" > 
                    <div className="column is-half coteDroit">
                    <h1>AUTHENTIFICATION</h1>
                    <br></br>
                    <h2>IDENTIFIANT</h2>
                        <div className="id">
                            <input
                            className="input is-rounded"
                            type="email"
                            id="email"
                            placeholder="Email"
                            required
                            />            
                        </div>
                        <h2>MOT DE PASSE</h2>
                        <div className="mdp">
                            <input
                            className="input is-rounded"
                            type="password"
                            id="password"
                            placeholder="Mot de passe"
                            required
                            />            
                        </div>
                        <br></br>
                        <a className={`button boutonConnexion`} href="/menuEleve">
                        SE CONNECTER
                        </a>
                        <br></br>

                        <div className='lien'>   
                        <a href="/lien" target="_blank">Première connexion / Mot de passe oublié</a>
                        </div>    
                    </div>
                    <Logo />  
                </div>
            </div>
        </div>
    );
};

export default Connexion;

