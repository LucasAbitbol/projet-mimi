import React from 'react';
import DropFileInput from '../componentsDragDrop/drop-file-input/DropFileInput';

const DepotCours = () => {

    const onFileChange = (files) => {
        console.log(files);
    }

    return(
        <div className="box">
            <h2 className="header">
                Veuillez déposer vos fichiers
            </h2>
            <br></br>
            <DropFileInput
                onFileChange={(files) => onFileChange(files)}
            />
        </div>
    );
};


export default DepotCours;
