import React from 'react';
import Navigation from "../components/Navigation";
import Navbar from '../components/navbar/index';

const MenuEleve = () => {
    return(
        <div className = "menu">
            <Navbar />
            <div className="columns is-full is-mobile is-vcentered total">
                <div className="column is-half ligne1">
                    <a href="/cours">
                    <img src="./img/dossier.png" alt="cours" style={{width:'150px', height:'150px;', marginRight:'150px'}}/>
                    </a>
                    <a href="/visio">
                    <img src="./img/video.png" alt="video" style={{width:'151px', height:'151px;', marginLeft:'150px'}}/>
                    </a>
                </div>

                <div className="texte">
                    <div className="texteCours">
                        <p>ACCEDER AUX COURS</p>
                    </div>
                    <div className="texteVisio">
                        <p>VISIOCONFERENCE</p>
                    </div>
                </div>

                <div className="column is-half ligne2">
                    <a href="/avatar">
                    <img src="./img/avatar.png" alt="avatar" style={{width:'150px', height:'150px;', marginRight:'150px'}}/>
                    </a>
                    <a href="/jeux">
                    <img src="./img/jeux.png" alt="jeux" style={{width:'150px', height:'150px;', marginLeft:'150px'}}/>
                    </a>
                </div>

                <div className="texte">
                    <div className="texteAvatar">
                        <p>CHOISIR SON AVATAR</p>
                    </div>
                    <div className="texteJeux">
                        <p>JOUER !</p>
                    </div>
                </div>
            </div>
        </div>
    );
};

export default MenuEleve;