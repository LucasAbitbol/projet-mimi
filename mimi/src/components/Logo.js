import React from 'react';

const Logo = () => {
    return(
        <div className = "logo">
           <img src="./img/logoCHL.png" alt="logo" />
        </div>
    );
};

export default Logo;