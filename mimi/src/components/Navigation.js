import React from 'react';
import { NavLink } from "react-router-dom"

const Navigation = () => {
    return(
        <div className = "navigation">
            <NavLink exact to="/" activeClassName="nav-active">
                AUTHENTIFICATION
            </NavLink>
            <NavLink exact to="menuEleve" activeClassName="nav-active">
                MENU
            </NavLink>
        </div>
    );
};

export default Navigation;